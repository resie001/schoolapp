package ade.resie.schoolapp.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ade.resie.schoolapp.R
import ade.resie.schoolapp.api.body.InsertJurusanBody
import ade.resie.schoolapp.api.body.UpdateJurusanBody
import ade.resie.schoolapp.databinding.FragmentJurusanDialogBinding
import ade.resie.schoolapp.model.Jurusan
import ade.resie.schoolapp.utils.Prefs
import ade.resie.schoolapp.utils.Status
import ade.resie.schoolapp.viewmodel.FactoryViewModel
import ade.resie.schoolapp.viewmodel.JurusanViewModel
import ade.resie.schoolapp.viewmodel.dialog.JurusanDialogViewModel
import android.content.Context
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider

class JurusanDialogFragment : DialogFragment(), View.OnClickListener {

    private lateinit var binding: FragmentJurusanDialogBinding
    private lateinit var appContext: Context
    private lateinit var progressDialogFragment: ProgressDialogFragment
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var prefs: Prefs

    private var jurusanId = 0

    private val viewModel: JurusanDialogViewModel by lazy {
        val factory = FactoryViewModel.getInstance(appContext)
        ViewModelProvider(this, factory).get(JurusanDialogViewModel::class.java)
    }

    private val subViewModel: JurusanViewModel by lazy {
        val factory = FactoryViewModel.getInstance(appContext)
        ViewModelProvider(requireActivity(), factory).get(JurusanViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = requireContext()
        supportFragmentManager = requireActivity().supportFragmentManager
        progressDialogFragment = ProgressDialogFragment.newInstance("Mohon tunggu sebentar...")
        progressDialogFragment.isCancelable = false
        prefs = Prefs(appContext)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_jurusan_dialog, container, false)
        defineUI(binding)
        defineViewModel(viewModel)
        return binding.root
    }

    private fun defineUI(ui: FragmentJurusanDialogBinding) = ui.apply {
        if (arguments?.getParcelable<Jurusan>(JURUSAN) != null){
            val jurusan = arguments?.getParcelable(JURUSAN) as Jurusan?
            tilKuota.editText?.setText("${jurusan?.penjurusanKuota ?: 0}")
            tilName.editText?.setText(jurusan?.penjurusanNama)
            switchStatus.isChecked = jurusan?.penjurusanStatus == 1
            jurusanId = jurusan?.penjurusanId ?: 0
            btnSet.text = "Update"
        }
        btnSet.setOnClickListener(this@JurusanDialogFragment)
    }

    private fun defineViewModel(viewModel: JurusanDialogViewModel) = viewModel.apply {
        response.observe(viewLifecycleOwner, {
            if (it != null){
                when(it.status){
                    Status.SUCCESS -> {
                        it.data.let { data ->
                            progressDialogFragment.dismiss()
                            if (data?.status == 200) {
                                subViewModel.getListJurusan()
                                dismiss()
                            }
                            else Toast.makeText(appContext, data?.message, Toast.LENGTH_LONG).show()
                        }
                    }
                    Status.ERROR -> {
                        progressDialogFragment.dismiss()
                        Toast.makeText(appContext, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> progressDialogFragment.show(supportFragmentManager, "Loading")
                }
            }
        })
    }

    companion object {

        private const val JURUSAN = "Jurusan"

        @JvmStatic
        fun newInstance(jurusan: Jurusan) =
            JurusanDialogFragment().apply {
                val bundle = Bundle()
                bundle.putParcelable(JURUSAN, jurusan)
                arguments = bundle
            }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onClick(v: View?) {
        if (v == binding.btnSet) check()
    }

    private fun check() = binding.apply{
        if (tilName.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Nama jurusan kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (tilKuota.editText?.text.isNullOrEmpty() || tilKuota.editText?.text.toString().toInt() == 0){
            Toast.makeText(appContext, "Kuota jurusan kosong atau nol!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        val status = if (switchStatus.isChecked) 1 else 0
        val token = "Bearer "+prefs.token
        if (arguments?.getParcelable<Jurusan>(JURUSAN) != null){
            val updateJurusanBody = UpdateJurusanBody(nama = tilName.editText?.text.toString(), kuota = tilKuota.editText?.text.toString().toInt(), status = status, id = jurusanId)
            viewModel.update(updateJurusanBody, token)
        } else {
            val insertBody = InsertJurusanBody(nama = tilName.editText?.text.toString(), kuota = tilKuota.editText?.text.toString().toInt(), status = status)
            viewModel.insert(insertBody, token)
        }
    }
}