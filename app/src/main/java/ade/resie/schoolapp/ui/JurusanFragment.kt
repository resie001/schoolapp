package ade.resie.schoolapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ade.resie.schoolapp.R
import ade.resie.schoolapp.adapter.JurusanAdapter
import ade.resie.schoolapp.databinding.FragmentJurusanBinding
import ade.resie.schoolapp.model.Jurusan
import ade.resie.schoolapp.ui.dialog.JurusanDialogFragment
import ade.resie.schoolapp.ui.dialog.ProgressDialogFragment
import ade.resie.schoolapp.utils.Status
import ade.resie.schoolapp.viewmodel.FactoryViewModel
import ade.resie.schoolapp.viewmodel.JurusanViewModel
import android.content.Context
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager

class JurusanFragment : Fragment(), View.OnClickListener {

    private lateinit var jurusanAdapter: JurusanAdapter
    private lateinit var binding: FragmentJurusanBinding
    private lateinit var progressDialogFragment: ProgressDialogFragment
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var appContext: Context

    private val viewModel: JurusanViewModel by lazy {
        val factory = FactoryViewModel.getInstance(appContext)
        ViewModelProvider(requireActivity(), factory).get(JurusanViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = requireContext()
        supportFragmentManager = requireActivity().supportFragmentManager
        progressDialogFragment = ProgressDialogFragment.newInstance("Mohon tunggu sebentar...")
        jurusanAdapter = JurusanAdapter { jurusan: Jurusan -> onClick(jurusan) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_jurusan, container, false)
        defineUI(binding)
        defineViewModel(viewModel)
        return binding.root
    }

    private fun defineUI(ui: FragmentJurusanBinding) = ui.apply {
        rvJurusan.apply {
            adapter = jurusanAdapter
            layoutManager = LinearLayoutManager(appContext)
        }
        floatingActionButton.setOnClickListener(this@JurusanFragment)
    }

    private fun defineViewModel(viewModel: JurusanViewModel) = viewModel.apply {
        getListJurusan()
        viewModel.response.observe(viewLifecycleOwner, {
            if (it != null){
                when(it.status){
                    Status.SUCCESS -> {
                        it.data.let { data ->
                            progressDialogFragment.dismiss()
                            if (data?.status == 200) {
                                val list = data.data ?: listOf()
                                jurusanAdapter.setData(list)
                                if (list.isEmpty()){
                                    binding.rvJurusan.visibility = View.GONE
                                    binding.tvEmpty.visibility = View.VISIBLE
                                } else {
                                    binding.rvJurusan.visibility = View.VISIBLE
                                    binding.tvEmpty.visibility = View.GONE
                                }
                            }
                            else Toast.makeText(appContext, data?.message, Toast.LENGTH_LONG).show()
                        }
                    }
                    Status.ERROR -> {
                        progressDialogFragment.dismiss()
                        Toast.makeText(appContext, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> progressDialogFragment.show(supportFragmentManager, "Loading")
                }
            }
        })
    }

    private fun onClick(jurusan: Jurusan) = JurusanDialogFragment.newInstance(jurusan).show(supportFragmentManager, "Jurusan Dialog")

    override fun onClick(v: View?) {
        if(v == binding.floatingActionButton) JurusanDialogFragment().show(supportFragmentManager, "Jurusan Dialog")
    }
}