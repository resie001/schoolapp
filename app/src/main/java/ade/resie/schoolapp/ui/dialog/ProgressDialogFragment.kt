package ade.resie.schoolapp.ui.dialog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ade.resie.schoolapp.R
import ade.resie.schoolapp.databinding.FragmentProgressDialogBinding
import android.app.Activity
import android.content.Context
import android.view.WindowManager
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

class ProgressDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentProgressDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_progress_dialog, container, false)
        binding.tvText.text = arguments?.getString(text)
        return binding.root
    }

    companion object {

        private const val text = "Text"

        @JvmStatic
        fun newInstance(param: String) =
            ProgressDialogFragment().apply {
                arguments = bundleOf(text to param)
                isCancelable = false
            }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }
}