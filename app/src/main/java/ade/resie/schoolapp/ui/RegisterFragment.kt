package ade.resie.schoolapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ade.resie.schoolapp.R
import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.databinding.FragmentRegisterBinding
import ade.resie.schoolapp.ui.dialog.ProgressDialogFragment
import ade.resie.schoolapp.utils.Status
import ade.resie.schoolapp.viewmodel.FactoryViewModel
import ade.resie.schoolapp.viewmodel.RegisterViewModel
import android.content.Context
import android.content.res.ColorStateList
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.textfield.TextInputLayout
import android.util.Patterns
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController

class RegisterFragment : Fragment(), View.OnClickListener, TextWatcher {

    private lateinit var binding: FragmentRegisterBinding
    private lateinit var appContext: Context
    private lateinit var progressDialogFragment: ProgressDialogFragment
    private lateinit var supportFragmentManager: FragmentManager

    private val viewModel: RegisterViewModel by lazy {
        val factory = FactoryViewModel.getInstance(appContext)
        ViewModelProvider(this, factory).get(RegisterViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = requireContext()
        supportFragmentManager = requireActivity().supportFragmentManager
        progressDialogFragment = ProgressDialogFragment.newInstance("Mohon tunggu sebentar...")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        defineUI(binding)
        defineViewModel(viewModel)
        (activity as AppCompatActivity).supportActionBar?.title = "Register"
        return binding.root
    }

    private fun defineUI(ui: FragmentRegisterBinding) = ui.apply {
        btnSignin.setOnClickListener(this@RegisterFragment)
        edPassword.addTextChangedListener(this@RegisterFragment)
    }

    private fun defineViewModel(viewModel: RegisterViewModel) = viewModel.apply {
        response.observe(viewLifecycleOwner, {
            if (it != null){
                when(it.status){
                    Status.SUCCESS -> {
                        it.data.let { data ->
                            progressDialogFragment.dismiss()
                            if (data?.status == 200) {
                                Toast.makeText(appContext, data.message, Toast.LENGTH_LONG).show()
                                findNavController().navigateUp()
                            }
                            else Toast.makeText(appContext, data?.message, Toast.LENGTH_LONG).show()
                        }
                    }
                    Status.ERROR -> {
                        progressDialogFragment.dismiss()
                        Toast.makeText(appContext, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> progressDialogFragment.show(supportFragmentManager, "Loading")
                }
            }
        })
    }

    override fun onClick(v: View?) {
        if (v == binding.btnSignin) check()
    }

    private fun check() = binding.apply{
        if (tilEmail.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Email kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (tilPassword.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Password kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (tilConfirm.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Konfirmasi password kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(tilEmail.editText?.text.toString()).matches()){
            Toast.makeText(appContext, "Format email salah!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (edPassword.text!!.length < 5){
            Toast.makeText(appContext, "Password minimal 5 huruf", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (edPassword.text.toString() != edConfirm.text.toString()){
            Toast.makeText(appContext, "Password dan Konfirmasi password tidak sama!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        viewModel.register(LoginBody(tilEmail.editText?.text.toString(), edPassword.text.toString()))
    }

    private fun validateInput(textInputLayout: TextInputLayout){
        val weak = ColorStateList(arrayOf(
            intArrayOf(-android.R.attr.state_enabled), intArrayOf(android.R.attr.state_enabled)),
            intArrayOf(ContextCompat.getColor(appContext, R.color.colorRed700), ContextCompat.getColor(appContext, R.color.colorRed700))
        )
        val normal = ColorStateList(arrayOf(
            intArrayOf(-android.R.attr.state_enabled), intArrayOf(android.R.attr.state_enabled)),
            intArrayOf(ContextCompat.getColor(appContext, R.color.colorYellow700), ContextCompat.getColor(appContext, R.color.colorYellow700))
        )
        val good = ColorStateList(arrayOf(
            intArrayOf(-android.R.attr.state_enabled), intArrayOf(android.R.attr.state_enabled)),
            intArrayOf(ContextCompat.getColor(appContext, R.color.colorGreen700), ContextCompat.getColor(appContext, R.color.colorGreen700))
        )
        when(checkPassword(textInputLayout.editText?.text!!)){
            0 -> {
                textInputLayout.apply {
                    helperText = "Password Lemah"
                    setHelperTextColor(weak)
                }
            }
            1 -> {
                textInputLayout.apply {
                    helperText = "Password Sedang"
                    setHelperTextColor(normal)
                }
            }
            2 -> {
                textInputLayout.apply {
                    helperText = "Password Kuat"
                    setHelperTextColor(good)
                }
            }
        }
    }

    private fun checkPassword(password: Editable) : Int{
        var angka = 0
        for ( item in password){
            if (item.isDigit()) {
                angka += 1
                break
            }
        }
        for ( item in password){
            if (item.isUpperCase()){
                angka += 1
                break
            }
        }
        return angka
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(s: Editable?) {
        when{
            binding.edPassword.hasFocus() -> validateInput(binding.tilPassword)
        }
    }
}