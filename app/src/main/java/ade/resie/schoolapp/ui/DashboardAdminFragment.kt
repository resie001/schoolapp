package ade.resie.schoolapp.ui

import ade.resie.schoolapp.R
import ade.resie.schoolapp.adapter.TabLayoutAdapter
import ade.resie.schoolapp.databinding.FragmentDashboardAdminBinding
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class DashboardAdminFragment : Fragment() {

    private lateinit var tabLayoutAdapter: TabLayoutAdapter
    private lateinit var binding: FragmentDashboardAdminBinding
    private lateinit var appActivity: Activity

    private val titles = arrayOf("Siswa","Jurusan")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appActivity = requireActivity()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard_admin, container, false)
        (activity as AppCompatActivity).supportActionBar?.title = "Dashboard"
        defineUi(binding)
        return binding.root
    }

    private fun defineUi(ui: FragmentDashboardAdminBinding) = ui.apply {
        tabLayoutAdapter = TabLayoutAdapter(appActivity as AppCompatActivity)
        ui.viewPagerGoods.adapter = tabLayoutAdapter
        TabLayoutMediator(
            ui.tabLayoutGoods, ui.viewPagerGoods
        ) { tab: TabLayout.Tab, position: Int -> tab.text = titles[position] }.attach()
    }
}