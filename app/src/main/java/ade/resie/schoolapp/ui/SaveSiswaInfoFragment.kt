package ade.resie.schoolapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ade.resie.schoolapp.R
import ade.resie.schoolapp.databinding.FragmentSaveSiswaInfoBinding
import ade.resie.schoolapp.ui.dialog.ProgressDialogFragment
import ade.resie.schoolapp.utils.Prefs
import ade.resie.schoolapp.utils.Status
import ade.resie.schoolapp.viewmodel.FactoryViewModel
import ade.resie.schoolapp.viewmodel.SaveSiswaInfoViewModel
import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.canhub.cropper.CropImage
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.io.File

class SaveSiswaInfoFragment : Fragment(), View.OnClickListener, EasyPermissions.PermissionCallbacks {

    private lateinit var binding: FragmentSaveSiswaInfoBinding
    private lateinit var appContext: Context
    private lateinit var appActivity: Activity
    private lateinit var progressDialogFragment: ProgressDialogFragment
    private lateinit var prefs: Prefs
    private lateinit var supportFragmentManager: FragmentManager

    private var uri: Uri? = null

    private val viewModel: SaveSiswaInfoViewModel by lazy {
        val factory = FactoryViewModel.getInstance(appContext)
        ViewModelProvider(this, factory).get(SaveSiswaInfoViewModel::class.java)
    }

    companion object {
        const val PERMISSION = 1000
        const val IMAGE = 1001
        const val PICTURE = 200
        val perms: Array<out String> =
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = requireContext()
        appActivity = requireActivity()
        supportFragmentManager = requireActivity().supportFragmentManager
        progressDialogFragment = ProgressDialogFragment.newInstance("Mohon tunggu sebentar...")
        prefs = Prefs(appContext)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_save_siswa_info, container, false)
        defineUI(binding)
        defineViewModel(viewModel)
        return binding.root
    }

    private fun defineUI(ui: FragmentSaveSiswaInfoBinding) = ui.apply {
        (activity as AppCompatActivity).supportActionBar?.title = "Data Siswa"
        btnAddPhoto.setOnClickListener(this@SaveSiswaInfoFragment)
        btnCameraTranscaction.setOnClickListener(this@SaveSiswaInfoFragment)
        btnCloseImage.setOnClickListener(this@SaveSiswaInfoFragment)
        btnSimpan.setOnClickListener(this@SaveSiswaInfoFragment)
    }

    private fun defineViewModel(viewModel: SaveSiswaInfoViewModel) = viewModel.apply {
        response.observe(viewLifecycleOwner, {
            if (it != null){
                when(it.status){
                    Status.SUCCESS -> {
                        it.data.let { data ->
                            progressDialogFragment.dismiss()
                            if (data?.status == 200) {
                                findNavController().navigateUp()
                                Toast.makeText(appContext, data.message, Toast.LENGTH_LONG).show()
                            }
                            else Toast.makeText(appContext, data?.message, Toast.LENGTH_LONG).show()
                        }
                    }
                    Status.ERROR -> {
                        progressDialogFragment.dismiss()
                        Toast.makeText(appContext, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> progressDialogFragment.show(supportFragmentManager, "Loading")
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when(v){
            binding.btnAddPhoto -> openGallery()
            binding.btnCameraTranscaction -> checkPermissionCamera()
            binding.btnCloseImage -> reset()
            binding.btnSimpan -> check()
        }
    }

    private fun check() = binding.apply{
        if (tilNama.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Nama kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (tilAlamat.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Alamat kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (tilNotelp.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Nomor Telepon kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (tilSekolahAsal.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Sekolah asal kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (tilNamaOrangTua.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Nama orang tua kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        if (uri == null) {
            Toast.makeText(appContext, "foto kosong!", Toast.LENGTH_SHORT).show()
            return@apply
        }
        viewModel.upload(prefs.id, tilNama.editText?.text.toString(), tilAlamat.editText?.text.toString(), tilNotelp.editText?.text.toString(), tilSekolahAsal.editText?.text.toString(), tilNamaOrangTua.editText?.text.toString(), uri!!, prefs.token!!)
    }

    private fun openImage() {
        binding.imgPhoto.visibility = View.VISIBLE
        binding.btnCloseImage.visibility = View.VISIBLE
        binding.dividerButton.visibility = View.GONE
    }

    private fun reset() {
        binding.imgPhoto.visibility = View.GONE
        binding.btnCloseImage.visibility = View.GONE
        binding.dividerButton.visibility = View.VISIBLE
        uri = null
    }

    private fun openGallery() {
        val i = Intent()
        i.type = "image/*"
        i.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(i, "Select Picture"), PICTURE)
    }

    private fun checkPermissionCamera() {
        if (EasyPermissions.hasPermissions(appContext, *perms)) {
            openCamera()
        } else {
            EasyPermissions.requestPermissions(
                this,
                "Aplikasi SchoolApp membutuhkan akses kamera dan memori untuk mengunggah foto dari kamera",
                PERMISSION,
                *perms
            )
        }
    }

    @AfterPermissionGranted(PERMISSION)
    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        uri = appActivity.contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            values
        )
        cameraIntent.putExtra(
            MediaStore.EXTRA_OUTPUT, uri
        )
        startActivityForResult(cameraIntent, IMAGE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICTURE && resultCode == Activity.RESULT_OK && data?.data != null) {
            val selectedImageUri: Uri = data.data!!
            CropImage.activity(selectedImageUri).start(appContext, this)
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK && result != null) {
                uri = Uri.fromFile(File(result.getUriFilePath(appContext, true)!!))
                Glide.with(this).load(result.uriContent).into(binding.imgPhoto)
                openImage()
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(
                    appContext,
                    "Potong gambar mengalami kesalahan!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        if (requestCode == IMAGE && resultCode == Activity.RESULT_OK) CropImage.activity(uri)
            .start(appContext, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {}

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            val dialog = AppSettingsDialog.Builder(this)
            dialog.apply {
                setTitle("Akses Kamera")
                setRationale("Anda telah memblokir permanen untuk akses kamera dan memori. Aplikasi Penguin membutuhkan akses kamera dan memori.")
                setPositiveButton("PENGATURAN")
                setNegativeButton("TUTUP")
            }
            dialog.build().show()
        }
    }

}