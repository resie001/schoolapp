package ade.resie.schoolapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ade.resie.schoolapp.R
import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.api.response.BaseResponse
import ade.resie.schoolapp.api.response.LoginResponse
import ade.resie.schoolapp.databinding.FragmentLoginBinding
import ade.resie.schoolapp.ui.dialog.ProgressDialogFragment
import ade.resie.schoolapp.utils.Prefs
import ade.resie.schoolapp.utils.Status
import ade.resie.schoolapp.viewmodel.FactoryViewModel
import ade.resie.schoolapp.viewmodel.LoginViewModel
import android.app.Activity
import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

class LoginFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var appContext: Context
    private lateinit var supportFragmentManager: FragmentManager
    private lateinit var appActivity: Activity
    private lateinit var progressDialogFragment: ProgressDialogFragment
    private lateinit var prefs: Prefs

    private val viewModel: LoginViewModel by lazy {
        val factory = FactoryViewModel.getInstance(appContext)
        ViewModelProvider(this, factory).get(LoginViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = requireContext()
        appActivity = requireActivity()
        supportFragmentManager = requireActivity().supportFragmentManager
        progressDialogFragment = ProgressDialogFragment.newInstance("Mohon tunggu sebentar...")
        prefs = Prefs(appContext)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        setupUI(binding)
        setupViewModel(viewModel)
        (activity as AppCompatActivity).supportActionBar?.title = "Login"
        return binding.root
    }

    private fun setupUI(ui: FragmentLoginBinding) = ui.apply {
        btnLogin.setOnClickListener(this@LoginFragment)
        btnDaftar.setOnClickListener(this@LoginFragment)
    }

    private fun setupViewModel(viewModel: LoginViewModel) = viewModel.apply {
        response.observe(viewLifecycleOwner, {
            if (it != null){
                when(it.status){
                    Status.SUCCESS -> {
                        it.data.let { data ->
                            progressDialogFragment.dismiss()
                            if (data?.status == 200) login(data)
                            else Toast.makeText(appContext, data?.message, Toast.LENGTH_LONG).show()
                        }
                    }
                    Status.ERROR -> {
                        progressDialogFragment.dismiss()
                        Toast.makeText(appContext, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> progressDialogFragment.show(supportFragmentManager, "Loading")
                }
            }
        })
    }

    private fun login(data: BaseResponse<LoginResponse>){
        prefs.token = data.data?.token
        prefs.id = data.data?.id ?: 0
        prefs.email = data.data?.email
        if (data.message == "Admin"){
            Toast.makeText(appContext, "Berhasil login sebagai admin", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_loginFragment_to_dashboardAdminFragment)
        } else {
            Toast.makeText(appContext, "Berhasil login sebagai siswa", Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.action_loginFragment_to_dashboardSiswaFragment)
        }
    }

    override fun onClick(v: View?) {
        when(v){
            binding.btnLogin -> check()
            binding.btnDaftar -> findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun check(){
        if (binding.tilEmail.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Belum memasukkan email!", Toast.LENGTH_LONG).show()
            return
        }
        if (binding.tilPassword.editText?.text.isNullOrEmpty()){
            Toast.makeText(appContext, "Belum memasukkan password!", Toast.LENGTH_LONG).show()
            return
        }
        val loginBody = LoginBody(email = binding.tilEmail.editText?.text.toString(), password = binding.tilPassword.editText?.text.toString())
        viewModel.login(loginBody)
    }

}