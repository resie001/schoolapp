package ade.resie.schoolapp.ui

import ade.resie.schoolapp.R
import ade.resie.schoolapp.api.response.SiswaResponse
import ade.resie.schoolapp.databinding.FragmentDashboardSiswaBinding
import ade.resie.schoolapp.ui.dialog.ProgressDialogFragment
import ade.resie.schoolapp.utils.Prefs
import ade.resie.schoolapp.utils.Status
import ade.resie.schoolapp.viewmodel.DashboardSiswaViewModel
import ade.resie.schoolapp.viewmodel.FactoryViewModel
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide

class DashboardSiswaFragment : Fragment(){

    private lateinit var binding: FragmentDashboardSiswaBinding
    private lateinit var appContext: Context
    private lateinit var progressDialogFragment: ProgressDialogFragment
    private lateinit var prefs: Prefs
    private lateinit var supportFragmentManager: FragmentManager

    private val viewModel: DashboardSiswaViewModel by lazy {
        val factory = FactoryViewModel.getInstance(appContext)
        ViewModelProvider(requireActivity(), factory).get(DashboardSiswaViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContext = requireContext()
        supportFragmentManager = requireActivity().supportFragmentManager
        progressDialogFragment = ProgressDialogFragment.newInstance("Mohon tunggu sebentar...")
        progressDialogFragment.isCancelable = true
        prefs = Prefs(appContext)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard_siswa, container, false)
        defineUI(binding)
        defineViewModel(viewModel)
        return binding.root
    }

    private fun defineUI(ui: FragmentDashboardSiswaBinding) = ui.apply {
        (activity as AppCompatActivity).supportActionBar?.title = "Dashbooard"
    }

    override fun onResume() {
        super.onResume()
        viewModel.getInfo(prefs.id, prefs.token!!)
    }

    private fun defineViewModel(viewModel: DashboardSiswaViewModel) = viewModel.apply {
        getInfo(prefs.id, prefs.token!!)
        response.observe(viewLifecycleOwner, {
            if (it != null){
                when(it.status){
                    Status.SUCCESS -> {
                        if (progressDialogFragment.isVisible) progressDialogFragment.dismiss()
                        it.data.let { data ->
                            if (data?.status == 200) check(data.data)
                            else Toast.makeText(appContext, data?.message, Toast.LENGTH_LONG).show()
                        }
                    }
                    Status.ERROR -> {
                        progressDialogFragment.dismiss()
                        Toast.makeText(appContext, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> progressDialogFragment.show(supportFragmentManager, "Loading")
                }
            }
        })
    }

    private fun check(siswaResponse: SiswaResponse?) = binding.apply{
        if (siswaResponse != null){
            if (siswaResponse.jurusan == null){
                imgPhoto.visibility = View.VISIBLE
                tvNama.visibility = View.VISIBLE
                btnStep.visibility = View.VISIBLE
                btnStep.text = "Pilih Jurusan"
                tvStatus.text = "Anda belum memilih jurusan"
                tvNama.text = siswaResponse.nama
                Glide.with(appContext).load(siswaResponse.foto).into(binding.imgPhoto)
                btnStep.setOnClickListener {
                    findNavController().navigate(R.id.action_dashboardSiswaFragment_to_saveSiswaInfoFragment)
                }
            } else {
                imgPhoto.visibility = View.VISIBLE
                tvNama.visibility = View.VISIBLE
                btnStep.visibility = View.VISIBLE
                btnStep.text = "Pilih Jurusan"
                tvStatus.text = "Anda telah masuk jurusan ${siswaResponse.jurusan}"
                tvNama.text = siswaResponse.nama
                Glide.with(appContext).load(siswaResponse.foto).into(binding.imgPhoto)
                btnStep.visibility = View.GONE
            }
        } else {
            imgPhoto.visibility = View.GONE
            tvNama.visibility = View.GONE
            btnStep.visibility = View.VISIBLE
            btnStep.text = "Daftar"
            tvStatus.text = "Anda belum mengisi data siswa"
            btnStep.setOnClickListener {
                findNavController().navigate(R.id.action_dashboardSiswaFragment_to_saveSiswaInfoFragment)
            }
        }
    }
}