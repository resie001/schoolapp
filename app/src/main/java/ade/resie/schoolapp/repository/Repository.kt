package ade.resie.schoolapp.repository

import ade.resie.schoolapp.api.ApiHelper
import ade.resie.schoolapp.api.ApiService
import ade.resie.schoolapp.api.body.InsertJurusanBody
import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.api.body.SaveSiswaInfoBody
import ade.resie.schoolapp.api.body.UpdateJurusanBody

class Repository(private val apiHelper: ApiHelper) {

    suspend fun login(loginBody: LoginBody) = apiHelper.login(loginBody)

    suspend fun register(loginBody: LoginBody) = apiHelper.register(loginBody)

    suspend fun getListJurusan() = apiHelper.getListJurusan()

    suspend fun insertJurusan(insertJurusanBody: InsertJurusanBody, token: String) = apiHelper.insertJurusan(insertJurusanBody, token)

    suspend fun updateJurusan(updateJurusanBody: UpdateJurusanBody, token: String) = apiHelper.updateJurusan(updateJurusanBody, token)

    suspend fun getInfoSiswa(id: Int, token: String) = apiHelper.getSiswaInfo(id, token)

    suspend fun saveSiswaInfo(saveSiswaInfoBody: SaveSiswaInfoBody, token: String) = apiHelper.saveSiswaInfo(saveSiswaInfoBody, token)

}