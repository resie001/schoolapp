package ade.resie.schoolapp.adapter

import ade.resie.schoolapp.ui.JurusanFragment
import ade.resie.schoolapp.ui.SiswaAdminFragment
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class TabLayoutAdapter(appActivity: AppCompatActivity) : FragmentStateAdapter(appActivity) {

    private val listFragment: List<Fragment> =
        listOf(
            SiswaAdminFragment(),
            JurusanFragment()
        )

    override fun getItemCount(): Int = listFragment.size

    override fun createFragment(position: Int): Fragment = listFragment[position]

}