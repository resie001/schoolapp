package ade.resie.schoolapp.adapter

import ade.resie.schoolapp.R
import ade.resie.schoolapp.databinding.ItemJurusanBinding
import ade.resie.schoolapp.model.Jurusan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView

class JurusanAdapter( private val clicklistener : (Jurusan) -> Unit): RecyclerView.Adapter<JurusanAdapter.ViewHolder>() {

    private val jurusan = mutableListOf<Jurusan>()

    fun setData(jurusan: List<Jurusan>){
        this.jurusan.clear()
        this.jurusan.addAll(jurusan)
        notifyDataSetChanged()
    }

    inner class ViewHolder(val ui: ItemJurusanBinding) : RecyclerView.ViewHolder(ui.root) {
        fun bind(data: Jurusan){
            ui.tvNama.text = data.penjurusanNama
            ui.tvKuota.text = "${data.penjurusanKuota} siswa"
            ui.btnEdit.setOnClickListener { clicklistener(data) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(DataBindingUtil.inflate(
        LayoutInflater.from(parent.context), R.layout.item_jurusan, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(jurusan[position])
    }

    override fun getItemCount(): Int = jurusan.size

}