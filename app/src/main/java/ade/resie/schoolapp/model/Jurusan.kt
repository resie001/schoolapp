package ade.resie.schoolapp.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Jurusan(
    @SerializedName("penjurusan_id")
    val penjurusanId: Int,
    @SerializedName("penjurusan_kuota")
    val penjurusanKuota: Int,
    @SerializedName("penjurusan_nama")
    val penjurusanNama: String,
    @SerializedName("penjurusan_status")
    val penjurusanStatus: Int
) : Parcelable