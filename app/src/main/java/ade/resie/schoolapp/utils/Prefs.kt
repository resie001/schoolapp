package ade.resie.schoolapp.utils

import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context) {

    private val schoolApp = "SchoolApp"
    private val _token = "token"
    private val _id = "id"
    private val _email = "email"

    private val preferences: SharedPreferences =
        context.getSharedPreferences(schoolApp, Context.MODE_PRIVATE)

    var token
        get() = preferences.getString(_token, "")
        set(value) = preferences.edit().putString(_token, value).apply()

    var email
        get() = preferences.getString(_email, "")
        set(value) = preferences.edit().putString(_email, value).apply()

    var id
        get() = preferences.getInt(_id, 0)
        set(value) = preferences.edit().putInt(_id, value).apply()

}