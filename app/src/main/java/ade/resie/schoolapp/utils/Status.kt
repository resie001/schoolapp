package ade.resie.schoolapp.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}