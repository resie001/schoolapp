package ade.resie.schoolapp.viewmodel

import ade.resie.schoolapp.api.ApiHelper
import ade.resie.schoolapp.api.RetrofitBuilder
import ade.resie.schoolapp.repository.Repository
import ade.resie.schoolapp.viewmodel.dialog.JurusanDialogViewModel
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class FactoryViewModel(private val repository: Repository): ViewModelProvider.Factory {

    companion object {
        @Volatile
        private var INSTANCE: FactoryViewModel? = null

        fun getInstance(context: Context): FactoryViewModel {
            var instance = INSTANCE
            if (instance == null) {
                instance = FactoryViewModel(Repository(ApiHelper(RetrofitBuilder.getApiService(context))))
                INSTANCE = instance
            }
            return instance
        }
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> {
                @Suppress("UNCHECKED_CAST")
                return LoginViewModel(repository) as T
            }
            modelClass.isAssignableFrom(JurusanViewModel::class.java) -> {
                @Suppress("UNCHECKED_CAST")
                return JurusanViewModel(repository) as T
            }
            modelClass.isAssignableFrom(JurusanDialogViewModel::class.java) -> {
                @Suppress("UNCHECKED_CAST")
                return JurusanDialogViewModel(repository) as T
            }
            modelClass.isAssignableFrom(RegisterViewModel::class.java) -> {
                @Suppress("UNCHECKED_CAST")
                return RegisterViewModel(repository) as T
            }
            modelClass.isAssignableFrom(DashboardSiswaViewModel::class.java) -> {
                @Suppress("UNCHECKED_CAST")
                return DashboardSiswaViewModel(repository) as T
            }
            modelClass.isAssignableFrom(SaveSiswaInfoViewModel::class.java) -> {
                @Suppress("UNCHECKED_CAST")
                return SaveSiswaInfoViewModel(repository) as T
            }
            else -> throw IllegalArgumentException("Unable to construct ViewModel")
        }
    }
}