package ade.resie.schoolapp.viewmodel.dialog

import ade.resie.schoolapp.api.body.InsertJurusanBody
import ade.resie.schoolapp.api.body.UpdateJurusanBody
import ade.resie.schoolapp.api.response.SimpleResponse
import ade.resie.schoolapp.repository.Repository
import ade.resie.schoolapp.utils.Resource
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class JurusanDialogViewModel(private val repository: Repository): ViewModel()  {

    private val _response = MutableLiveData<Resource<SimpleResponse>>()
    val response: LiveData<Resource<SimpleResponse>>
        get() = _response

    fun insert(insertJurusanBody: InsertJurusanBody, token: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _response.postValue(Resource.loading(data = null))
                try {
                    _response.postValue(Resource.success(data = repository.insertJurusan(insertJurusanBody, token)))
                } catch (exception: Exception) {
                    _response.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
                }
            }
        }
    }

    fun update(updateJurusanBody: UpdateJurusanBody, token: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _response.postValue(Resource.loading(data = null))
                try {
                    _response.postValue(Resource.success(data = repository.updateJurusan(updateJurusanBody, token)))
                } catch (exception: Exception) {
                    _response.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
                }
            }
        }
    }

}