package ade.resie.schoolapp.viewmodel

import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.api.body.SaveSiswaInfoBody
import ade.resie.schoolapp.api.response.BaseResponse
import ade.resie.schoolapp.api.response.LoginResponse
import ade.resie.schoolapp.api.response.SimpleResponse
import ade.resie.schoolapp.repository.Repository
import ade.resie.schoolapp.utils.Resource
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.net.URI

class SaveSiswaInfoViewModel(private val repository: Repository): ViewModel() {

    private var storage: StorageReference =
        FirebaseStorage.getInstance("gs://schoolapp-243a1.appspot.com").reference

    private val _response = MutableLiveData<Resource<SimpleResponse>>()
    val response: LiveData<Resource<SimpleResponse>>
        get() = _response

    private fun save(saveSiswaInfoBody: SaveSiswaInfoBody, token: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    _response.postValue(Resource.success(data = repository.saveSiswaInfo(saveSiswaInfoBody, token)))
                } catch (exception: Exception) {
                    _response.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
                }
            }
        }
    }

    fun upload(userId: Int, nama: String, alamat: String, notelp: String, sekolahAsal: String, namaOrangTua: String, uri: Uri, token: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _response.postValue(Resource.loading(data = null))
                storage.child(userId.toString()).child(uri.pathSegments.last()).putFile(uri)
                    .addOnSuccessListener {
                        it.storage.downloadUrl.addOnSuccessListener { downloadUrl ->
                            val saveSiswaInfoBody = SaveSiswaInfoBody(nama, downloadUrl.toString(), alamat, notelp, sekolahAsal, namaOrangTua, userId)
                            save(saveSiswaInfoBody, token)
                        }
                    }.addOnFailureListener {
                        _response.postValue(Resource.error(data = null, message = it.message ?: "Error Occurred!"))
                    }
            }
        }
    }
}