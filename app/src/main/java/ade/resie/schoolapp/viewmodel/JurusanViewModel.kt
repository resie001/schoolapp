package ade.resie.schoolapp.viewmodel

import ade.resie.schoolapp.api.body.InsertJurusanBody
import ade.resie.schoolapp.api.response.BaseResponse
import ade.resie.schoolapp.api.response.SimpleResponse
import ade.resie.schoolapp.model.Jurusan
import ade.resie.schoolapp.repository.Repository
import ade.resie.schoolapp.utils.Resource
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class JurusanViewModel(private val repository: Repository): ViewModel() {

    private val _response = MutableLiveData<Resource<BaseResponse<List<Jurusan>>>>()
    val response: LiveData<Resource<BaseResponse<List<Jurusan>>>>
        get() = _response

    fun getListJurusan() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _response.postValue(Resource.loading(data = null))
                try {
                    _response.postValue(Resource.success(data = repository.getListJurusan()))
                } catch (exception: Exception) {
                    _response.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
                }
            }
        }
    }

}