package ade.resie.schoolapp.viewmodel

import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.api.response.BaseResponse
import ade.resie.schoolapp.api.response.LoginResponse
import ade.resie.schoolapp.api.response.SimpleResponse
import ade.resie.schoolapp.repository.Repository
import ade.resie.schoolapp.utils.Resource
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegisterViewModel(private val repository: Repository): ViewModel() {

    private val _response = MutableLiveData<Resource<SimpleResponse>>()
    val response: LiveData<Resource<SimpleResponse>>
        get() = _response

    fun register(loginBody: LoginBody) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _response.postValue(Resource.loading(data = null))
                try {
                    _response.postValue(Resource.success(data = repository.register(loginBody)))
                } catch (exception: Exception) {
                    _response.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
                }
            }
        }
    }

}