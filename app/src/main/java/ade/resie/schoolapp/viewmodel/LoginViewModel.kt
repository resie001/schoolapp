package ade.resie.schoolapp.viewmodel

import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.api.response.BaseResponse
import ade.resie.schoolapp.api.response.LoginResponse
import ade.resie.schoolapp.repository.Repository
import ade.resie.schoolapp.utils.Resource
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(private val repository: Repository): ViewModel() {

    private val _response = MutableLiveData<Resource<BaseResponse<LoginResponse>>>()
    val response: LiveData<Resource<BaseResponse<LoginResponse>>>
        get() = _response

    fun login(loginBody: LoginBody) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _response.postValue(Resource.loading(data = null))
                try {
                    _response.postValue(Resource.success(data = repository.login(loginBody)))
                } catch (exception: Exception) {
                    _response.postValue(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
                }
            }
        }
    }

}