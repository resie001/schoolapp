package ade.resie.schoolapp.api

import ade.resie.schoolapp.api.body.InsertJurusanBody
import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.api.body.SaveSiswaInfoBody
import ade.resie.schoolapp.api.body.UpdateJurusanBody
import ade.resie.schoolapp.api.response.BaseResponse
import ade.resie.schoolapp.api.response.LoginResponse
import ade.resie.schoolapp.api.response.SimpleResponse
import ade.resie.schoolapp.api.response.SiswaResponse
import ade.resie.schoolapp.model.Jurusan
import retrofit2.http.*

interface ApiService {

    @POST("auth/login")
    suspend fun login(@Body loginBody: LoginBody): BaseResponse<LoginResponse>

    @POST("auth/register")
    suspend fun register(@Body loginBody: LoginBody): SimpleResponse

    @GET("penjurusan")
    suspend fun getListJurusan(): BaseResponse<List<Jurusan>>

    @POST("penjurusan")
    suspend fun insertJurusan(@Body insertJurusanBody: InsertJurusanBody, @Header("Authorization") token: String): SimpleResponse

    @PUT("penjurusan")
    suspend fun updateJurusan(@Body updateJurusanBody: UpdateJurusanBody, @Header("Authorization") token: String): SimpleResponse

    @GET("pendaftaran/{id}")
    suspend fun getSiswaInfo(@Path("id") id: Int, @Header("Authorization") token: String): BaseResponse<SiswaResponse>

    @POST("pendaftaran")
    suspend fun saveSiswaInfo(@Body saveSiswaInfoBody: SaveSiswaInfoBody, @Header("Authorization") token: String): SimpleResponse

}