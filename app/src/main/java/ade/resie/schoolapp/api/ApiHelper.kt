package ade.resie.schoolapp.api

import ade.resie.schoolapp.api.body.InsertJurusanBody
import ade.resie.schoolapp.api.body.LoginBody
import ade.resie.schoolapp.api.body.SaveSiswaInfoBody
import ade.resie.schoolapp.api.body.UpdateJurusanBody

class ApiHelper(private val apiService: ApiService) {

    suspend fun login(loginBody: LoginBody) = apiService.login(loginBody)

    suspend fun register(loginBody: LoginBody) = apiService.register(loginBody)

    suspend fun getListJurusan() = apiService.getListJurusan()

    suspend fun insertJurusan(insertJurusanBody: InsertJurusanBody, token: String) = apiService.insertJurusan(insertJurusanBody, token)

    suspend fun updateJurusan(updateJurusanBody: UpdateJurusanBody, token: String) = apiService.updateJurusan(updateJurusanBody, token)

    suspend fun getSiswaInfo(id: Int, token: String) = apiService.getSiswaInfo(id, token)

    suspend fun saveSiswaInfo(saveSiswaInfoBody: SaveSiswaInfoBody, token: String) = apiService.saveSiswaInfo(saveSiswaInfoBody, token)

}