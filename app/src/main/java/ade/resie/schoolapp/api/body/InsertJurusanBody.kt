package ade.resie.schoolapp.api.body

data class InsertJurusanBody(
    private val nama: String,
    private val kuota: Int,
    private val status: Int
)
