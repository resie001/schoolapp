package ade.resie.schoolapp.api.response

import com.google.gson.annotations.SerializedName

data class SiswaResponse(
    @SerializedName("csiswa_nama")
    val nama: String,
    @SerializedName("csiswa_foto")
    val foto: String,
    @SerializedName("penjurusan_nama")
    val jurusan: String? = null
)
