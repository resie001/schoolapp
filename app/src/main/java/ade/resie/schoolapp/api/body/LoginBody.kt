package ade.resie.schoolapp.api.body

data class LoginBody(
    private val email: String,
    private val password: String
)
