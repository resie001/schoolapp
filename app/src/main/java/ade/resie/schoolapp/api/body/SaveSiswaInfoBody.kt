package ade.resie.schoolapp.api.body

data class SaveSiswaInfoBody(
    private val nama: String,
    private val foto: String,
    private val alamat: String,
    private val notelp: String,
    private val sekolah_asal: String,
    private val nama_orang_tua: String,
    private val userid: Int
)
