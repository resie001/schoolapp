package ade.resie.schoolapp.api

import ade.resie.schoolapp.R
import android.content.Context
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private fun getRetrofit(context: Context): Retrofit {
        return Retrofit.Builder()
            .baseUrl(context.getString(R.string.base_url))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getApiService(context: Context) = getRetrofit(context).create(ApiService::class.java)
}