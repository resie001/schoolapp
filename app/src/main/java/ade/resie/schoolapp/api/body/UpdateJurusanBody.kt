package ade.resie.schoolapp.api.body

data class UpdateJurusanBody(
    private val nama: String,
    private val kuota: Int,
    private val status: Int,
    private val id: Int,
)
