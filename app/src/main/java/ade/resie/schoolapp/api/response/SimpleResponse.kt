package ade.resie.schoolapp.api.response

import com.google.gson.annotations.SerializedName

data class SimpleResponse(
    @SerializedName("status")
    val status: Int,
    @SerializedName("message")
    val message: String)
